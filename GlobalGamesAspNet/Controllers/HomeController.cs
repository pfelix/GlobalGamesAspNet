﻿using GlobalGamesAspNet.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace GlobalGamesAspNet.Controllers
{
    public class HomeController : Controller
    {

        DataClasses1DataContext dc = new DataClasses1DataContext();

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Sobre()
        {
            return View();
        }

        public ActionResult Servicos()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Servicos(Orcamento orcamento)
        {
            if (ModelState.IsValid)
            {
                dc.Orcamento.InsertOnSubmit(orcamento);
            }

            try
            {
                dc.SubmitChanges();

                return RedirectToAction("Index");
            }
            catch (Exception)
            {
                return View();
            }
        }
    }
}