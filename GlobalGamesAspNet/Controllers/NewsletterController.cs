﻿using GlobalGamesAspNet.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace GlobalGamesAspNet.Controllers
{
    public class NewsletterController : Controller
    {
        DataClasses1DataContext dc = new DataClasses1DataContext();

        // GET: Newsletter
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Newsletter(Newsletter newsletter)
        {
            if(ModelState.IsValid)
            {
                dc.Newsletter.InsertOnSubmit(newsletter);
            }

            try
            {
                dc.SubmitChanges();

                return RedirectToAction("Index", "Home");
            }
            catch
            {
                return View();
            }
        }
    }
}
